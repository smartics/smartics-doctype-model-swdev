<?xml version='1.0'?>
<!--

    Copyright 2013-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="use-case"
  base-template="standard"
  provide-type="standard-type"
  category="design">
  <resource-bundle>
    <l10n>
      <name>Use Case</name>
      <description>
        Document the use cases of your system.
      </description>
      <about>
        Defines how users interact with the system. Use cases may also set
        interactions with the system into a broader context.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Anwendungsfälle">Anwendungsfall</name>
      <description>
        Beschreiben Sie einen Anwendungsfall.
      </description>
      <about>
        Dokumentieren Sie wie Anwender mit Ihrem System arbeiten.
        Anwendungsfälle zeigen die Interaktion der Anwender mit dem System in
        einem breiteren Kontext.
      </about>
      <type plural="Typen von Anwendungsfällen">Typ eines Anwendungsfalls</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.use-case.id">
      <value>
        <macro name="projectdoc-transclusion-property-display">
          <param name="property-name" key="projectdoc.doctype.common.autoIdentifier"/>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Identifier</name>
          <description>
            Define the unique identifier of the use case.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Identifikator</name>
          <description>
            Definieren Sie einen eindeutigen Identifikator für den Anwendungsfall.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">use-case-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.use-case.primary-actors">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">role</param>
          <param
            name="property"
            key="projectdoc.doctype.use-case.primary-actors" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Primary Actors</name>
        </l10n>
        <l10n locale="de">
          <name>Primäre Akteure</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.use-case.secondary-actors">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">role</param>
          <param
            name="property"
            key="projectdoc.doctype.use-case.secondary-actors" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Secondary Actors</name>
        </l10n>
        <l10n locale="de">
          <name>Sekundäre Akteure</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.use-case.supporting-actors">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">role</param>
          <param
            name="property"
            key="projectdoc.doctype.use-case.supporting-actors" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Supporting Actors</name>
        </l10n>
        <l10n locale="de">
          <name>Unterstützende Akteure</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.use-case.level">
      <value>
        <macro name="projectdoc-use-case-level" />
      </value>
      <resource-bundle>
        <l10n>
          <name>Goal Level</name>
        </l10n>
        <l10n locale="de">
          <name>Zielebene</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.use-case.frequency">
      <resource-bundle>
        <l10n>
          <name>Frequency</name>
          <description>
            Define the expected frequency of use. Dependent on the product this
            may be numbers like uses per visit/task of one user or uses per
            hour/day/week/month (of one/all user(s)). It is not important to
            compare two use cases by their frequency, it is important for the
            developer to get to know how often the function will be used by
            users of the system.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Häufigkeit</name>
          <description>
            Halten Sie die erwartete Häufigkeit f\u00fcr die Anwendung dieses
            Falls durch Benutzer Ihres Systems fest. Die Zahl soll Ihnen helfen
            beurteilen zu können, wie wichtig der Anwendungsfall für das System
            ist. Abhängig vom Produkt kann die Einheit unterschiedlich
            ausfallen: Verwendung pro Besuch oder Aufgabe eines Benutzers oder
            Verwendungen pro Zeiteinheit.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.level">
      <value>
        <macro name="projectdoc-level-macro" />
      </value>
    </property>
    <property key="projectdoc.doctype.common.sponsors">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder,organization,person,role</param>
          <param
            name="property"
            key="projectdoc.doctype.common.sponsors" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="empty-as-none">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Provide a description that sets the use case in a context without
            describing the steps of the use case.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Anwendungsfall in seinem Kontext, ohne auf die
            einzelnen Schritte konkret einzugehen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.summary">
      <resource-bundle>
        <l10n>
          <name>Summary</name>
          <description>
            Give a summary of the use case.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Zusammenfassung</name>
          <description>
            Fassen Sie die Bedeutung des Anwendungsfalls prägnant zusammen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.preconditions">
      <resource-bundle>
        <l10n>
          <name>Preconditions</name>
          <description>
            List the preconditions that have to be met that the use case can be
            triggered. You may use a table with two columns to name the
            precondition and describe it.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Vorbedingungen</name>
          <description>
            Beschreiben Sie die Vorbedingungen, die erfüllt sein müssen, damit
            dieser Anwendungsfall durchgeführt werden kann. Eine tabellarische
            Auflistung mit Name der Vorbedingung und einer kurzen Beschreibung
            ist hier oft ausreichend. Bei komplexeren Bedingungen kann jede
            Bedingung in einem eigenen Absatz beschrieben werden.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.postconditions">
      <resource-bundle>
        <l10n>
          <name>Postconditions</name>
          <description>
            List the postcondition that will be met after the use case has been
            executed. The minimal postcondition is that is always met, all
            other conditions may be met in certain circumstances that will be
            specified. You may use a three column table to specify the name,
            the circumstances in which case the postcondition gets active and a
            description of the condition.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Nachbedingungen</name>
          <description>
            Beschreiben Sie die Bedingungen, die nach der Ausführung des
            Anwendungsfalls erfüllt sind. Unterscheiden Sie zwischen der
            minimalen Nachbedingung, die in jedem Fall erfüllt ist von denen,
            die nur unter bestimmten Umständen erreicht werden. Es bietet sich
            eine dreispaltige tabellarische Auflistung an mit dem Namen der
            Nachbedingung, die Umstände unter denen sie eintritt und eine
            Beschreibung der Nachbedingung. Bei komplexeren Bedingungen kann
            jede Bedingung in einem eigenen Absatz beschrieben werden.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.triggers">
      <resource-bundle>
        <l10n>
          <name>Triggers</name>
          <description>
            List the triggers that initiate the use case. Use a two-column
            table with a name of the trigger and a description.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Auslöser</name>
          <description>
            Führen Sie die Auslöser auf, die zur Ausführung des Anwendungsfalls
            führen. Verwenden Sie eine zweispaltige Tabelle mit einem Namen des
            Auslösers und seiner Beschreibung. Bei komplexeren Auslösern kann
            jeder Auslöser in einem eigenen Absatz beschrieben werden.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.basic-course">
      <resource-bundle>
        <l10n>
          <name>Basic Course</name>
          <description>
            Describe the central course of events.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Standardablauf</name>
          <description>
            Beschreiben Sie den Hauptablaufpfad durch den Anwendungsfalls. Dies
            ist i.d.R. der Happy-Path: Der Anwender durchlauft den Fall
            geradlinig zu seinem Ziel.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.alternative-sequences">
      <resource-bundle>
        <l10n>
          <name>Alternative Sequences</name>
          <description>
            List the alternative, non-exceptional courses of events.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Alternative Abläufe</name>
          <description>
            Beschreiben Sie die alternativen Abläufe, die ebenfalls als
            gewöhnlich bezeichnet werden können. Dies sind wichtige Varianten
            des Standardablaufs.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.exception-sequences">
      <resource-bundle>
        <l10n>
          <name>Exception Sequences</name>
          <description>
            List the exceptional courses of events.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Ausnahmeabläufe</name>
          <description>
            Beschreiben Sie die Abläufe, die auf außergewöhnliche Situation
            reagieren.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.extension-points">
      <resource-bundle>
        <l10n>
          <name>Extension Points</name>
          <description>
            List the extension points defined by this use case. These
            references describe the extension of the use case that extends
            another one.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Erweiterungspunkte</name>
          <description>
            Beschreiben Sie die Erweiterungspunkte des Anwendungsfalls und
            unter welchen Bedingungen die Erweiterung verfügbar ist.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.technology-and-data-variations">
      <resource-bundle>
        <l10n>
          <name>Technology and Data Variations</name>
          <description>
            List the variations of the sequence due to technology or data
            constraints.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Technische und datenbedingte Variationen</name>
          <description>
            Beschreiben Sie Varianten des Ablaufs, die durch Technologien oder
            Daten bedingt sind.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.business-rules">
      <resource-bundle>
        <l10n>
          <name>Business Rules</name>
          <description>
            List the business rules related to the use case.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Geschäftsregeln</name>
          <description>
            Listen Sie die Geschäftsregeln auf, die mit diesem Anwendungsfall
            verbunden sind.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.use-case.stakeholders">
      <resource-bundle>
        <l10n>
          <name>Stakeholders</name>
          <description>
            List the stakeholders that have interests in the use case.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Stakeholder</name>
          <description>
            Listen Sie die Stakeholder auf, die an diesem Anwendungsfall
            interessiert sind.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="architecture-decision" />
    <doctype-ref id="architecture-alternative" />
    <doctype-ref id="component" />
    <doctype-ref id="view" />
    <doctype-ref id="user-character" />
    <doctype-ref id="role" />
  </related-doctypes>
</doctype>
