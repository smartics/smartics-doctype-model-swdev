<?xml version='1.0'?>
<!--

    Copyright 2013-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="out-item"
  base-template="standard"
  provide-type="standard-type"
  category="analysis">
  <resource-bundle>
    <l10n>
      <name>Out Item</name>
      <description>
        Document what is out of scope of your project.
      </description>
      <about>
        Out items record topics that are out of scope of your project. To
        document them is useful for project inception and for later reference.
        Scopes in agile projects may change quickly. Therefore it is helpful to
        recognize the current context for your decisions.
      </about>
    </l10n>
    <l10n locale="de">
      <name>Out-Item</name>
      <description>
        Dokumentieren Sie Punkte, die außerhalb des Projektziels liegen.
      </description>
      <about>
        Dokumentieren Sie Punkte, die außerhalb des Projektziels liegen. Diese
        hilft dem Team, sich auf die wesentlichen Punkte zu konzentrieren und
        dokumentiert die Entscheidungen für einen späteren Zeitpunkt.
      </about>
      <type plural="Out-Item-Typen">Out-Item-Typ</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.out-item.id">
      <value>
        <macro name="projectdoc-transclusion-property-display">
          <param name="property-name" key="projectdoc.doctype.common.autoIdentifier"/>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Identifier</name>
          <description>
            Define the unique identifier of the out item.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Identifikator</name>
          <description>
            Definieren Sie einen eindeutigen Identifikator für das Out-Item.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">out-item-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.common.sponsors">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder,organization,person,role</param>
          <param
            name="property"
            key="projectdoc.doctype.common.sponsors" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="empty-as-none">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the item that is not part of the project goals.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie ein Feature, das nicht Teil des Projektziels ist.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="project-vision" />
    <doctype-ref id="project-constraint" />
    <doctype-ref id="requirement" />
    <doctype-ref id="feature" />
    <doctype-ref id="quality-scenario" />
    <doctype-ref id="user-character" />
    <doctype-ref id="role" />
  </related-doctypes>
</doctype>
