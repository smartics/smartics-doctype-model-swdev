<?xml version='1.0'?>
<!--

    Copyright 2013-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="test-case"
  base-template="standard"
  provide-type="standard-type"
  category="test">
  <resource-bundle>
    <l10n>
      <name>Test Case</name>
      <description>
        Document a test case of the project.
      </description>
      <about>
        Test cases document especially exploratory tests that have not yet been
        or will never be automated.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Testfälle">Testfall</name>
      <description>
        Dokumentieren Sie einen Testfall Ihres Projekts.
      </description>
      <about>
        Testfälle dokumentieren insbesondere explorative Tests, die noch nicht
        oder niemals automatisiert werden.
      </about>
      <type plural="Testfalltypen">Testfalltyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.test-case.id">
      <value>
        <macro name="projectdoc-transclusion-property-display">
          <param name="property-name" key="projectdoc.doctype.common.autoIdentifier"/>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Identifier</name>
          <description>
            Define the unique identifier of the test case.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Identifikator</name>
          <description>
            Definieren Sie einen eindeutigen Identifikator für den Testfall.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">test-case-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.test-case.source">
      <resource-bundle>
        <l10n>
          <name>Source</name>
          <description>
            Point to the source that implements the test case.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Quelle</name>
          <description>
            Referenzieren Sie die Quellen, die diesen Testfall implementieren.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.test-case.latestResult">
      <resource-bundle>
        <l10n>
          <name>Result</name>
          <description>
            Point to the result of the latest test run.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Ergebnis</name>
          <description>
            Referenzieren Sie das aktuelle Ergebnis des Testlaufs.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.sponsors">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder,organization,person,role</param>
          <param
            name="property"
            key="projectdoc.doctype.common.sponsors" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="empty-as-none">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe the context of your test case.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie den Kontext für Ihre Testfälle.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.test-case.suite">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.test-case.suite"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-section">
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.test-case.test"/></ac:parameter>
            <ac:parameter ac:name="level">*</ac:parameter>
            <ac:rich-text-body>
              <ac:placeholder><at:i18n at:key="projectdoc.doctype.test-case.test.placeholder"/></ac:placeholder>
            </ac:rich-text-body>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-section">
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.test-case.expected"/></ac:parameter>
            <ac:parameter ac:name="level">*</ac:parameter>
            <ac:rich-text-body>
              <ac:placeholder><at:i18n at:key="projectdoc.doctype.test-case.expected.placeholder"/></ac:placeholder>
            </ac:rich-text-body>
          </ac:structured-macro>

          <ac:structured-macro ac:name="section">
            <ac:rich-text-body>
              <ac:structured-macro ac:name="column">
                <ac:rich-text-body>
                  <ac:structured-macro ac:name="projectdoc-section">
                    <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.test-case.test-cases"/></ac:parameter>
                    <ac:parameter ac:name="level">*</ac:parameter>
                    <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.test-case.test-cases.intro"/></ac:parameter>
                    <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
                    <ac:rich-text-body>
                      <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
                        <ac:rich-text-body>
                          <p>
                            <ac:structured-macro ac:name="create-from-template">
                              <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:projectdoc-blueprint-doctype-test-case</ac:parameter>
                              <ac:parameter ac:name="buttonLabel"><at:i18n at:key="de.smartics.projectdoc.swdev.blueprint.test-case.create.label"/></ac:parameter>
                            </ac:structured-macro>
                          </p>
                        </ac:rich-text-body>
                      </ac:structured-macro>
                      <ac:structured-macro ac:name="projectdoc-display-table">
                        <ac:parameter ac:name="doctype">test-case</ac:parameter>
                        <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.iteration"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
                        <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
                        <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                        <ac:parameter ac:name="render-mode">definition</ac:parameter>
                        <ac:parameter ac:name="render-classes">test-cases-table, display-table, test-cases</ac:parameter>
                      </ac:structured-macro>
                    </ac:rich-text-body>
                  </ac:structured-macro>
                </ac:rich-text-body>
              </ac:structured-macro>
              <ac:structured-macro ac:name="column">
                <ac:rich-text-body>
                  <ac:structured-macro ac:name="projectdoc-section">
                    <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.test-case.test-datas"/></ac:parameter>
                    <ac:parameter ac:name="level">*</ac:parameter>
                    <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.test-case.test-datas.intro"/></ac:parameter>
                    <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
                    <ac:rich-text-body>
                      <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
                        <ac:rich-text-body>
                          <p>
                            <ac:structured-macro ac:name="create-from-template">
                              <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:projectdoc-blueprint-doctype-test-data</ac:parameter>
                              <ac:parameter ac:name="buttonLabel"><at:i18n at:key="de.smartics.projectdoc.swdev.blueprint.test-data.create.label"/></ac:parameter>
                            </ac:structured-macro>
                          </p>
                        </ac:rich-text-body>
                      </ac:structured-macro>
                      <ac:structured-macro ac:name="projectdoc-display-table">
                        <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
                        <ac:parameter ac:name="doctype">test-data</ac:parameter>
                        <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                        <ac:parameter ac:name="render-mode">definition</ac:parameter>
                        <ac:parameter ac:name="render-classes">test-datas-table, display-table, test-datas</ac:parameter>
                      </ac:structured-macro>
                    </ac:rich-text-body>
                  </ac:structured-macro>
                </ac:rich-text-body>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Test Suite</name>
          <intro>The test cases for this suite.</intro>
          <label key="projectdoc.doctype.test-case.test">Test</label>
          <label key="projectdoc.doctype.test-case.test.placeholder">
            Define the test.
          </label>
          <label key="projectdoc.doctype.test-case.expected">Expected Result</label>
          <label key="projectdoc.doctype.test-case.expected.placeholder">
            Describe the expected result.
          </label>
          <label key="projectdoc.doctype.test-case.test-cases">Test Cases</label>
          <label key="projectdoc.doctype.test-case.test-cases.intro">
            The test cases for this suite.
          </label>
          <label key="projectdoc.doctype.test-case.test-datas">Test Data</label>
          <label key="projectdoc.doctype.test-case.test-datas.intro">
            The test data common for all test cases in this suite.
          </label>
        </l10n>
        <l10n locale="de">
          <name>Test-Suite</name>
          <intro>The test cases for this suite.</intro>
          <label key="projectdoc.doctype.test-case.test">Test</label>
          <label key="projectdoc.doctype.test-case.test.placeholder">
            Definieren Sie den Testfall.
          </label>
          <label key="projectdoc.doctype.test-case.expected">Erwartetes Ergebnis</label>
          <label key="projectdoc.doctype.test-case.expected.placeholder">
            Beschreiben Sie das erwartete Ergebnis.
          </label>
          <label key="projectdoc.doctype.test-case.test-cases">Testfälle</label>
          <label key="projectdoc.doctype.test-case.test-cases.intro">
            Die Testfälle dieser Gruppe
          </label>
          <label key="projectdoc.doctype.test-case.test-datas">Testdaten</label>
          <label key="projectdoc.doctype.test-case.test-datas.intro">
            Testdaten als Grundlage für die Testfälle.
          </label>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="test-charter" />
    <doctype-ref id="test-data" />
    <doctype-ref id="test-report" />
    <doctype-ref id="test-session" />
  </related-doctypes>
</doctype>
