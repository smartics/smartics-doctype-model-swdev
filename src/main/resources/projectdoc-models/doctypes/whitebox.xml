<?xml version='1.0'?>
<!--

    Copyright 2013-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="whitebox"
  base-template="standard"
  provide-type="standard-type"
  category="design"
  has-homepage="false">
  <resource-bundle>
    <l10n>
      <name plural="Whiteboxes">Whitebox</name>
      <description>
        Provide a whitebox description for an element of the architecture.
      </description>
      <about>
        Describe as a whitebox the elements of a view where only the relations
        of internal elements are relevant.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Whiteboxen">Whitebox</name>
      <description>
        Erstellen Sie eine Beschreibung eines Elements Ihrer Architektur in
        seiner Whiteboxsicht.
      </description>
      <about>
        Beschreiben Sie mit einer Whiteboxsicht die Beziehungen der in einem
        Element enthaltenen Subelemente.
      </about>
      <type plural="Whiteboxtypen">Whiteboxtyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.parent">
      <value>
        <macro name="projectdoc-transclusion-parent-property">
          <param name="property-name" key="projectdoc.doctype.common.name" />
          <param name="parent-doctype">blackbox</param>
          <param name="property" key="projectdoc.doctype.common.parent" />
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">whitebox-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.common.level">
      <value>
        <macro name="projectdoc-level-macro" />
      </value>
    </property>
    <property key="projectdoc.doctype.common.reference">
      <resource-bundle>
        <l10n>
          <description>
            Provide a reference to this whitebox on a remote information
            system.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Referenzieren Sie weitere Informationen zu dieser Whitebox auf
            einem anderen Informationssystem.
          </description>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Give a summary on the whitebox.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Fassen Sie die Bedeutung der Whiteboxsicht zusammen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.whitebox.diagram">
      <resource-bundle>
        <l10n>
          <name>Diagram</name>
          <description>
            Add a diagram to describe the whitebox's internal structure.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Diagramm</name>
          <description>
            Fügen Sie ein Diagramm hinzu, um die Internas des Elements zu
            beschreiben.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.whitebox.blackboxes">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.whitebox.blackboxes"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">false</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="create-from-template">
            <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:projectdoc-blueprint-doctype-blackbox</ac:parameter>
            <ac:parameter ac:name="buttonLabel"><at:i18n at:key="de.smartics.projectdoc.swdev.blueprint.blackbox.create.label"/></ac:parameter>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">blackbox</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="render-classes">blackboxes-table, display-table, blackboxes</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Blackboxes</name>
        </l10n>
        <l10n locale="de">
          <name>Blackboxen</name>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.whitebox.internal-interfaces">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.whitebox.internal-interfaces"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="create-from-template">
            <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:projectdoc-blueprint-doctype-interface</ac:parameter>
            <ac:parameter ac:name="buttonLabel"><at:i18n at:key="de.smartics.projectdoc.swdev.blueprint.interface.create.label"/></ac:parameter>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">interface</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="render-classes">internal-interfaces-table, display-table, internal-interfaces</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Internal Interfaces</name>
        </l10n>
        <l10n locale="de">
          <name>Interne Schnittstellen</name>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.whitebox.architecture-decisions">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.whitebox.architecture-decisions"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="create-from-template">
            <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:projectdoc-blueprint-doctype-architecture-decision</ac:parameter>
            <ac:parameter ac:name="buttonLabel"><at:i18n at:key="de.smartics.projectdoc.swdev.blueprint.architecture-decision.create.label"/></ac:parameter>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">architecture-decision</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="render-classes">architecture-decisions-table, display-table, architecture-decisions</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Architecture Decisions</name>
        </l10n>
        <l10n locale="de">
          <name>Architekturentscheidungen</name>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.whitebox.special-dependency-issues">
      <resource-bundle>
        <l10n>
          <name>Special Dependency Issues</name>
          <description>
            Discuss selected complex dependencies of the local building blocks.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Spezielle Abhängigkeiten</name>
          <description>
            Diskutieren Sie die komplexen Abhängigkeiten der lokalen Bausteine.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.whitebox.open-issues">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.whitebox.open-issues"/></ac:parameter>
        <ac:parameter ac:name="ignore-template-buttons">true</ac:parameter>
        <ac:rich-text-body>
          <!-- Requires smartics-projectdoc-confluence-space-prjmgmt
          <ac:structured-macro ac:name="create-from-template">
            <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-prjmgmt:projectdoc-prjmgmt-blueprint-doctype-open-issue</ac:parameter>
            <ac:parameter ac:name="buttonLabel"><at:i18n at:key="de.smartics.projectdoc.swdev.blueprint.open-issue.create.label"/></ac:parameter>
          </ac:structured-macro>
           -->
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">open-issue</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="render-classes">open-issues-table, display-table, open-issues</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Open Issues</name>
        </l10n>
        <l10n locale="de">
          <name>Offene Punkte</name>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="component" />
    <doctype-ref id="view" />
    <doctype-ref id="blackbox" />
  </related-doctypes>

  <wizard template="minimal-params" form-id="test-session-form">
    <field><xml><![CDATA[      <div class="field-group">
        <label for="projectdoc_doctype_common_name">{getText('projectdoc.blueprint.form.label.name')}</label>
        <input id="projectdoc_doctype_common_name" class="text long-field" type="text" name="projectdoc_doctype_common_name"
        placeholder="{getText('projectdoc.blueprint.form.label.name.placeholder')}"  value="@parentPageTitle - {getText('projectdoc.doctype.blackbox.whitebox')}">
		  </div>]]></xml></field>
    <field template="short-description" />
  </wizard>
</doctype>
