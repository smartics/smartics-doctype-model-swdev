<?xml version='1.0'?>
<!--

    Copyright 2013-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="quality-target"
  base-template="standard"
  provide-type="standard-type"
  category="analysis">
  <resource-bundle>
    <l10n>
      <name>Quality Target</name>
      <description>
        Document information about expected quality targets in your products.
      </description>
      <about>
        Quality targets describe the desired primary aspects of your product.
        Making them visible to everyone on your team helps to stay focused and
        share a collective vision of your product.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Qualitätsziele">Qualitätsziel</name>
      <description>
        Dokumentieren Sie Informationen zu den geforderten Qualitätszielen in
        Ihrem Projekt.
      </description>
      <about>
        Qualitätsziele beschreiben übergeordnete Anforderungen an Ihr Produkt.
        Indem Sie diese Qualitätsziele für alle Team-Mitglieder beschreiben,
        schärfen Sie das gemeinsame Verständnis des Teams über das Produkt.
      </about>
      <type plural="Qualitätszieltypen">Qualitätszieltyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">quality-target-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.quality-target.priority">
      <resource-bundle>
        <l10n>
          <name>Priority</name>
          <description>
            Rank your quality targets and make sure that none of them have
            equals priority.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Priorität</name>
          <description>
            Definieren Sie eine Rangordnung ihrer Qualitätsziele und stellen
            Sie sicher, dass alle Ziele einer eigenen Priorität zugeordnet sind.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.quality-target.quality">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">quality</param>
          <param
            name="property"
            key="projectdoc.doctype.quality-target.quality" />
          <param name="name"><![CDATA[<at:var at:name="projectdoc_doctype_common_name"/>]]></param>
          <param name="empty-as-none">true</param>
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Quality</name>
        </l10n>
        <l10n locale="de">
          <name>Qualität</name>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe why this quality is relevant for the project.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie, warum diese Qualität ein wichtiges Ziel für das
            Projekt ist.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.common.summary">
      <resource-bundle>
        <l10n>
          <description>
            Provide a brief summary for the quality target.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Geben Sie eine kurze Zusammenfassung für das Qualitätsziel.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.quality-target.influencing-factors">
      <resource-bundle>
        <l10n>
          <name>Influencing Factors</name>
          <description>
            List the factors that have an influence on this quality target. You
            may mention other quality targets here or add additional influences
            that are specific for your project.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Beeinflussende Faktoren</name>
          <description>
            Nennen Sie Faktoren, die dieses Qualitätsziel beeinflussen. Sie
            können andere Qualitätsziele hier referenzieren oder zusätzliche
            Einflussfaktoren benennen, die spezifisch für Ihr Projekt sind.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.quality-target.solution-approach">
      <resource-bundle>
        <l10n>
          <name>Solution Approach</name>
          <description>
            Define the your approach to achieve this quality target.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Lösungsansatz</name>
          <description>
            Skizzieren Sie Ihren Lösungsansatz, um das gesteckte Qualitätsziel
            zu erreichen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.quality-target.requirements">
      <resource-bundle>
        <l10n>
          <name>Requirements</name>
          <description>
            List the requirements that led to defining the importance of this
            quality for the project.
          </description>
          <intro>
            The following requirements are the basis for defining the
            importance of this quality for the project.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Anforderungen</name>
          <description>
            Zählen Sie die Anforderungen auf, die zu der Bewertung der Qualität
            für das Projekt führten.
          </description>
          <intro>
            Die folgenden Anforderungen bilden die Basis für die Bewertung der
            Qualität für dieses Projekt.
          </intro>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.quality-target.topics">
      <resource-bundle>
        <l10n>
          <name>Topics</name>
          <description>
            Provide information and concepts organized into topics that are
            relevant to understand and handle this quality target.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Topics</name>
          <description>
            Listen Sie hier Informationen und Konzepte, die für das Verständnis
            der Qualitätsziele für dieses Projekt relevant sind.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="quality" />
    <doctype-ref id="quality-scenario" />
    <doctype-ref id="user-character" />
    <doctype-ref id="role" />
  </related-doctypes>
</doctype>
