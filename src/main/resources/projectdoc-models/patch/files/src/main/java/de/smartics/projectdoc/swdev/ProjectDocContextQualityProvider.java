/*
 * Copyright 2013-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.swdev;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContextKeys;
import com.atlassian.confluence.util.i18n.I18NBean;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocContextProvider;
import org.apache.commons.lang3.StringUtils;

/**
 * Extends for injection.
 */
public class ProjectDocContextQualityProvider
    extends ProjectDocContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectDocContextQualityProvider(
      final ContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final BlueprintContext context =
        super.updateBlueprintContext(blueprintContext);

    adjustTitle(blueprintContext);

    updateContextFinally(context);

    return context;
  }

  private void adjustTitle(final BlueprintContext blueprintContext) {
    final String title = String.valueOf(
        blueprintContext.get(BlueprintContextKeys.CONTENT_PAGE_TITLE.key()));
    if (StringUtils.isNotBlank(title)) {
      final I18NBean i18n = user.createI18n();
      final String labelForQuality =
          i18n.getText("projectdoc.quality.template.title");
      if (StringUtils.isNotBlank(labelForQuality)) {
        final String newTitle = title + " (" + labelForQuality + ')';
        blueprintContext.setTitle(newTitle);
      }
    }
  }

  // --- object basics --------------------------------------------------------

}
