require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
  "use strict";

  AJS.bind("blueprint.wizard-register.ready", function () {
    Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-session',
      function (wizard) {
        wizard.on('pre-render.page1Id', function (e, state) {
          state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
        });

        wizard.on('submit.page1Id', function (e, state) {
          const name = state.pageData["projectdoc_doctype_common_name"];
          if (!name) {
            alert('Please provide a name for this document.');
            return false;
          }

          PROJECTDOC.adjustToLocation(state);
        });

        wizard.on('post-render.page1Id', function (e, state) {
          const title = state.wizardData.title;
          if (title) {
            AJS.$('#projectdoc_doctype_common_name').val(title);
          }
        });

        wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
      });
  });
});
