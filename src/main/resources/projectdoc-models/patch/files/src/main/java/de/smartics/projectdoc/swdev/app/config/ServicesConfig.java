/*
 * Copyright 2013-2025 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.swdev.app.config;

import de.smartics.projectdoc.swdev.ProjectDocContextQualityProvider;
import de.smartics.projectdoc.swdev.ProjectDocDayContextProvider;
import de.smartics.projectdoc.swdev.subspace.PartitionContextProvider;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ConfluenceComponentImports.class, ProjectdocComponentImports.class,
    AppConfluenceComponentImports.class})
public class ServicesConfig {
  @Bean
  public ProjectDocContextQualityProvider projectDocContextQualityProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocContextQualityProvider(support);
  }

  @Bean
  public ProjectDocDayContextProvider ProjectDocDayContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocDayContextProvider(support);
  }

  @Bean
  public PartitionContextProvider PartitionContextProvider(
      final ContextProviderSupportService support) {
    return new PartitionContextProvider(support);
  }
}
