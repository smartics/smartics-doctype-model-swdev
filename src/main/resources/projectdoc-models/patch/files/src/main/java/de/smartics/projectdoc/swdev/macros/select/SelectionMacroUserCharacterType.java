/*
 * Copyright 2013-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.swdev.macros.select;

import com.atlassian.sal.api.message.I18nResolver;

/**
 * Allows to select a type for a user character.
 */
public class SelectionMacroUserCharacterType extends SelectionMacro
{
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   *
   * @param i18n provides access to localized information.
   */
  public SelectionMacroUserCharacterType(final I18nResolver i18n)
  {
    super(i18n);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected String getName()
  {
    return "projectdoc-user-character-type";
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
