projectdoc Doctypes for Software Development
============================================


## Overview

This is a free add-on for [projectdoc](https://www.smartics.eu/confluence/display/PDAC1/) for Confluence.

It provides the blueprints to create pages for

  * Components, Views, Use Cases, Technical Debts, Architecture Aspects
  * Project Constraints, Requirements, User Characters, Vision Statements
  * Quality Scenarios, Quality Targets
  * Properties
  * Artifacts, Nodes, Environments
  * and some more!

It also provides a space blueprint to get started with your software documentation project quickly.

## Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The projectdoc Core Doctypes Add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/PDAC1/Software+Development+Doctypes)
  * the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev)
